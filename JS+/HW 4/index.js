function fetchFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        data.forEach(film => {
            const filmInfo = document.createElement('div');
            filmInfo.innerHTML = `<h2>Епізод ${film.episodeId}: ${film.name}</h2>
                                  <p>${film.openingCrawl}</p>`;
            document.body.appendChild(filmInfo);

            fetchCharactersForFilm(film.characters, filmInfo);
        });
    })
    .catch(error => console.error('Помилка отримання даних про фільми:', error));
}


function fetchCharactersForFilm(characters, filmInfo) {
    Promise.all(characters.map(characterURL => fetch(characterURL)))

    .then(responses => Promise.all(responses.map(response => response.json())))
    .then((charactersData) => {
        const ul = document.createElement('ul');
        charactersData.forEach(character => {
        const li = document.createElement('li');
        li.textContent = character.name;
        ul.appendChild(li);
        });
        filmInfo.appendChild(ul);
    })

    .catch(error => console.error('Помилка отримання даних про персонажів:', error));
}
  
window.onload = fetchFilms;