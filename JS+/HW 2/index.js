const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];


const root = document.getElementById("root");
const ul = document.createElement("ul");

books.forEach(book => {
    try {
        if (!book.author) {
            throw new SyntaxError("В об'єкті відсутня властивість 'author'");
        }

        if (!book.name) {
            throw new SyntaxError("В об'єкті відсутня властивість 'name'");
        }

        if (!book.price) {
            throw new SyntaxError("В об'єкті відсутня властивість 'price'");
        }

        const li = document.createElement("li");
        li.textContent = `${book.author} : ${book.name} ( ${book.price} грн. ).`;
        ul.appendChild(li);

    } catch (error) {
        console.error(error.name, error.message);
    }
});

root.appendChild(ul);