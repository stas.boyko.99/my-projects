class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { method: 'DELETE' })
            .then(response => {
                if (response.ok) {
                    document.getElementById(this.post.id).remove();
                }
            })
            .catch(error => console.error('Error:', error));
    }

    render() {
        const card = document.createElement('div');
        card.id = this.post.id;
        card.className = 'card';
        card.innerHTML = `
            <h2>${this.post.title}</h2>
            <p>${this.post.body}</p>
            <p>Posted by: ${this.user.name} ${this.user.username} - ${this.user.email}</p>
            <button>Delete Post</button>
        `;
        card.querySelector('button').addEventListener('click', () => this.deleteCard());
        return card;
    }
}


function fetchData() {
    Promise.all([
        fetch('https://ajax.test-danit.com/api/json/users').then(res => res.json()),
        fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
    ]).then(([users, posts]) => {
        const usersMap = new Map(users.map(user => [user.id, user]));
        const postsContainer = document.getElementById('posts-container');
        posts.forEach(post => {
            const user = usersMap.get(post.userId);
            const card = new Card(post, user).render();
            postsContainer.appendChild(card);
        });
    }).catch(error => console.error('Error fetching data:', error));
}

document.addEventListener('DOMContentLoaded', fetchData);
