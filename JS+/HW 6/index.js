//      ТЕОРИТИЧНІ ПИТАННЯ
// Асинхронність в JS - це коли функції та операції виконуються не в тої послідовності
//   в якій вони прописані в коді, а залежно від дій корістувача.





const findIpBtn = document.getElementById('findIpBtn');
const infoDiv = document.getElementById('info');

async function getIP() {
  
  try {
    const response = await fetch("https://api.ipify.org/?format=json");
    
    const data = await response.json();
    return data.ip;
  }

  catch (error) {
      
    throw error;
      
  }
}


async function getIpInfo(ip) {
  
  try {
    const response = await fetch(`http://ip-api.com/json/${ip}`);
    const data = await response.json();
    return data;
  }

  catch (error) {
    if (!error instanceof TypeError) {
        throw error;
    }
  }
}


async function findIp() {
  try {
    const ip = await getIP();
    const ipInfo = await getIpInfo(ip);

    infoDiv.innerHTML = `
    <p><strong>Континент:</strong> ${ipInfo.continent || "Невідомо"}</p>
    <p><strong>Країна:</strong> ${ipInfo.country || "Невідомо"}</p>
    <p><strong>Регіон:</strong> ${ipInfo.regionName || "Невідомо"}</p>
    <p><strong>Місто:</strong> ${ipInfo.city || "Невідомо"}</p>
    <p><strong>Район:</strong> ${ipInfo.district || "Невідомо"}</p>
    `;
  }

  catch (error) {
      console.error(error);
  }
}


findIpBtn.addEventListener("click", findIp);


