

import React, { useState } from "react";

import ModalFooter from './components/Modal/ModalFooter';
import Modal from "./components/Modal/Modal";

import Fon from './public/images/fon.webp';


function App() {

  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  return (
    <>

      <ModalFooter 
        firstText = "Open first modal" 
        firstClick = {() => setFirstModalOpen(true)}

        secondaryText = "Open second modal" 
        secondaryClick = {() => setSecondModalOpen(true)}

        classNamesFirst = "cancel-btn"
        classNamesSecond = "delte-btn"
      />

      <Modal 
        isOpen={firstModalOpen} 
        onClose={() => setFirstModalOpen(false)} 
        title="Product Delete!" 
        text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted." 
        children={<img src={Fon} className='firstModalImg' alt="Image" />}

        footerProps={{
          
          firstText: "NO, CANCEL",
          secondaryText: "YES, DELETE",

          firstClick: () => console.log('Click'),
          secondaryClick: () => console.log('Click'),

          classNamesFirst: "cancel-btn",
          classNamesSecond: "delte-btn"
        }}
      />

      <Modal 
        isOpen={secondModalOpen} 
        onClose={() => setSecondModalOpen(false)} 
        title="Add Product “NAME”" 
        text="Description for you product" 

        footerProps={{
          firstText: "NO, CANCEL",
          firstClick: () => console.log('Click'),
          classNamesFirst: "cancel-btn"
        }}
      />

      
      
    </>
  )
}

export default App
