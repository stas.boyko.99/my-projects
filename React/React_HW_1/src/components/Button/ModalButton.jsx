

import './ModalButton.scss';

function ModalButton({ type="submit", classNames, onClick, children }) {
    return(
        
        <button type={type} className={classNames} onClick={onClick}>{children}</button>
        
    )
}


export default ModalButton;