

import './Modal.scss';

import ModalWrapper from './ModalWrapper';
import ModalClose from './ModalClose';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';



function Modal ({ isOpen, onClose, title, text, children, footerProps }) {
  if (!isOpen) return null;

  return (
    <ModalWrapper onClose={onClose}>
      
      <ModalClose onClick={onClose}/>

      {children}

      <ModalHeader children={title}/>

      <ModalBody children={text}/>

      <ModalFooter {...footerProps} />
      
    </ModalWrapper>
  );
};
  

export default Modal;

