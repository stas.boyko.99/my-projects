

import ModalButton from '../Button/ModalButton';

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick, classNamesFirst, classNamesSecond }) {
    return (
        
        <div className="button-container">

            {firstText && <ModalButton classNames={classNamesFirst} onClick={firstClick} children={firstText}/>}
            
            {secondaryText && <ModalButton classNames={classNamesSecond} onClick={secondaryClick} children={secondaryText}/>}
        
        </div>

    );
};
  
export default ModalFooter;