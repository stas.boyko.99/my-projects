

import ModalButton from '../Button/ModalButton'; 

function ModalClose({onClick}) {
    return (

        <ModalButton children='X' classNames="closelImg" onClick={onClick}/>

    )
}


export default ModalClose;