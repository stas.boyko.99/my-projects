
import { useState } from 'react'
import Api from './components/Api'
import Image from './components/Image'

function App() {


  const [isShown, setIsShown] = useState(false);

  return (
    <>
      <Api/>
      {isShown ? <Image/> : null}
      <button onClick={() => setIsShown(!isShown)}>Hide</button>
    </>
  )
}

export default App
