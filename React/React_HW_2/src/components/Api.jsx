
import { useEffect, useState } from "react";

function Api() {

    const [endpoint, setEndpoint] = useState('posts');
    const [data, setData] = useState('');

    useEffect(() => {
        const url = `https://jsonplaceholder.typicode.com/${endpoint}`;
        fetch(url)
            .then(response => response.json())
            .then(data => setData(data));
    }, [endpoint])

    const options = [
        'posts', 'photos', 'users', 'todos'
    ]

    function selectHandler(event) {
        const value = event.target.value;
        console.log(value);
        setEndpoint(value);
    }

    return(
        <div>
            <textarea value={JSON.stringify(data)} readOnly></textarea>
            <select onChange={selectHandler}>
                {options.map((item, index) => <option value={item} key={index}>{item}</option>)}
            </select>
        </div>
    )
}

export default Api;